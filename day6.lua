local utils = require'utils'


local Light = {

    new = function(self)
        self.__index = self
        local light = setmetatable({on=false, brightness=0}, self)
        return light
    end,

    toggle = function(self)
        self.on = not self.on
        self.brightness = self.brightness + 2
    end,
    turn_on = function(self)
        self.on = true
        self.brightness = self.brightness + 1
    end,
    turn_off = function(self)
        self.on = false
        if self.brightness > 0 then self.brightness = self.brightness - 1 end
    end
}


local function create_grid()
    local grid = {}
    for y=0, 999 do
        grid[y] = {}
        for x=0, 999 do
            grid[y][x] = Light:new()
        end
    end
    return grid
end

local function parse_instruction(instruction)
    return instruction:match('([%a%s]+) (%d+),(%d+) through (%d+),(%d+)')
end

local function execute_instruction(grid, instruction)
    local parsed, from_x, from_y, to_x, to_y = parse_instruction(instruction)
    for y=from_y, to_y do
        for x=from_x, to_x do
            local replaced, _ = parsed:gsub(' ', '_')
            grid[y][x][replaced](grid[y][x])
        end
    end
    return grid
end

local function count_on(grid)
    local count = 0
    for y=0, 999 do
        for x=0, 999 do
            if grid[y][x].on then count = count + 1 end
        end
    end
    return count
end

local function sum_brightness(grid)
    local brightness = 0
    for y=0, 999 do
        for x=0, 999 do
            brightness = brightness + grid[y][x].brightness
        end
    end
    return brightness
end

local instructions = utils.read_file_multi('inputs/day6.txt')
local grid = create_grid()
for _, instruction in ipairs(instructions) do
    grid = execute_instruction(grid, instruction)
end

assert(count_on(grid) == 377891)
assert(sum_brightness(grid) == 14110788)
