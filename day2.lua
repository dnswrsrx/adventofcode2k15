local utils = require'utils'

local function _parse(dimensions)
    local values = {}
    for char in dimensions:gmatch('%d+') do
        table.insert(values, char)
    end
    return values
end

local function calculate_wrapping_paper(dimension)
    local areas = {
        (dimension[1]*dimension[2]),
        (dimension[2]*dimension[3]),
        (dimension[1]*dimension[3])
    }
    local slack = math.min(table.unpack(areas))
    return (2*utils.sum(areas)) + slack
end

local function calculate_ribbon(dimension)
    table.sort(dimension, function(a, b) return tonumber(a) < tonumber(b) end)
    return (2*(dimension[1] + dimension[2])) + utils.product(dimension)
end

local function total_amount(dimensions)
    local sum_wrapping_paper = 0
    local sum_ribbon = 0
    for _, raw_dimension in ipairs(dimensions) do
        local dimension = _parse(raw_dimension)
        sum_wrapping_paper = sum_wrapping_paper + calculate_wrapping_paper(dimension)
        sum_ribbon = sum_ribbon + calculate_ribbon(dimension)
    end
    return {sum_wrapping_paper, sum_ribbon}
end


local tests = {
    ['2x3x4'] = {58, 34},
    ['1x1x10'] = {43, 14}
}

for case, amount in pairs(tests) do
    local parsed = _parse(case)
    assert (calculate_wrapping_paper(parsed) == amount[1])
    assert (calculate_ribbon(parsed) == amount[2])
end

local dimensions = utils.read_file_multi('inputs/day2.txt')
assert(table.concat(total_amount(dimensions), ',') == '1606483,3842356')
