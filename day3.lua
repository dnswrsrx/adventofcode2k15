local utils = require'utils'

local Deliverer = {
    new = function(self)
        self.__index = self
        local deliverer = setmetatable({x=0, y=0}, self)
        return deliverer
    end,

    __call = function(self, move)
        if move == '^' then
            self.y = self.y + 1
        elseif move == 'v' then
            self.y = self.y - 1
        elseif move == '<' then
            self.x = self.x - 1
        elseif move == '>' then
            self.x = self.x + 1
        end
    end
}

local function travel(moves)
    local deliverer = Deliverer:new()

    local two_deliverers = {Deliverer:new(), Deliverer:new()}

    local houses = {['0,0']=true}
    local number_of_houses = 1

    local houses_with_two = {['0,0']=true}
    local number_of_houses_with_two = 1

    for i=1, #moves do
        local move = moves:sub(i, i)
        deliverer(move)
                
        local address = string.format('%s,%s', deliverer.x, deliverer.y)
        if houses[address] == nil then
            number_of_houses = number_of_houses + 1
            houses[address] = true
        end

        local current_deliverer = two_deliverers[(i%2)+1]
        current_deliverer(move)
                
        address = string.format('%s,%s', current_deliverer.x, current_deliverer.y)
        if houses_with_two[address] == nil then
            number_of_houses_with_two = number_of_houses_with_two + 1
            houses_with_two[address] = true
        end
    end
    return {number_of_houses, number_of_houses_with_two}
end

local cases = {
    ['>'] = {2, 2},
    ['^>v<'] = {4, 3},
    ['^v^v^v^v^v'] = {2, 11},
    ['^v'] = {2, 3}
}

for case, numbers in pairs(cases) do
    for i, v in ipairs(travel(case)) do
        assert(numbers[i] == v)
    end
end

local results = travel(utils.read_file_single('inputs/day3.txt'))
assert(results[1] == 2572)
assert(results[2] == 2631)
