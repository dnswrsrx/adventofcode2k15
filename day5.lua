local utils = require'utils'

local DISALLOWED = {'ab', 'cd', 'pq', 'xy'}

local function any_disallowed(sequence)
    for _, disallowed in ipairs(DISALLOWED) do
        if sequence:find(disallowed) then return true end
    end
end

local VOWELS = {'a', 'e', 'i', 'o', 'u'}

local function enough_vowels(sequence)
    local found = 0
    for _, vowel in ipairs(VOWELS) do
        local _, count = sequence:gsub(vowel, '')
        found = found + count
        if found > 2 then return true end
    end
end

local function has_consecutive_repeat(sequence)
    local characters = {}
    for char in sequence:gmatch('.') do characters[char] = true end
    for character, _ in pairs(characters) do
        if sequence:find(character..character) then return true end
    end
end

local function is_nice_v1(sequence)
    return (
        not any_disallowed(sequence) and
        enough_vowels(sequence) and
        has_consecutive_repeat(sequence)
    )
end

local function recurring_pairs(sequence)
    for i=1, #sequence-1 do
        local _, count = sequence:gsub(sequence:sub(i, i+1), '')
        if count > 1 then return true end
    end
end

local function interleaved_pair(sequence)
    for i=1, #sequence-2 do
        if sequence:sub(i, i) == sequence:sub(i+2, i+2) then return true end
    end
end

local function is_nice_v2(sequence)
    return recurring_pairs(sequence) and interleaved_pair(sequence)
end

local function count_nice(sequences)
    local count_v1 = 0
    local count_v2 = 0
    for _, sequence in ipairs(sequences) do
        if is_nice_v1(sequence) then count_v1 = count_v1 + 1 end
        if is_nice_v2(sequence) then count_v2 = count_v2 + 1 end
    end
    return {count_v1, count_v2}
end

local cases = {
    ugknbfddgicrmopn = true,
    aaa = true,
    jchzalrnumimnmhp = nil,
    haegwjzuvuyypxyu = nil,
    dvszwmarrgswjxmb = nil
}

for case, nice in pairs(cases) do
    assert(is_nice_v1(case) == nice)
end

local v2_cases = {
    qjhvhtzxzqqjkmpb = true,
    xxyxx = true,
    uurcxstgmygtbstg = nil,
    ieodomkazucvgmuy = nil
}

for case, nice in pairs(v2_cases) do
    assert(is_nice_v2(case) == nice)
end

local sequences = utils.read_file_multi('inputs/day5.txt')
local counts = count_nice(sequences)
assert(counts[1] == 236)
assert(counts[2] == 51)
