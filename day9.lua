local utils = require('utils')


local function parse_distances(distances)
    local parsed = {}
    local locations = {}

    for _, distance in ipairs(distances) do
        local departure, arrival, length = distance:match('(%a+) to (%a+) = (%d+)')

        for permutation in utils.permutations({departure, arrival}) do
            local location_1, location_2 = table.unpack(permutation)

            if parsed[location_1] == nil then
                table.insert(locations, location_1)
                parsed[location_1] = {}
            end
            parsed[location_1][location_2] = tonumber(length)
        end
    end

    return parsed, locations
end


local function calculate_distance(locations, length_map)
    local length = 0
    for i=1, #locations-1 do
        length = length_map[locations[i]][locations[i+1]] + length
    end
    return length
end

local function distances_for_permutations(locations, length_map)
    local lengths = {}
    for permutation in utils.permutations(locations) do
        table.insert(lengths, calculate_distance(permutation, length_map))
    end
    return lengths
end


local tests = {
    'London to Dublin = 464',
    'London to Belfast = 518',
    'Dublin to Belfast = 141'
}

local length_map, locations = parse_distances(tests)
local lengths = distances_for_permutations(locations, length_map)
assert(math.min(table.unpack(lengths)) == 605)
assert(math.max(table.unpack(lengths)) == 982)

length_map, locations = parse_distances(utils.read_file_multi('./inputs/day9.txt'))
lengths = distances_for_permutations(locations, length_map)
assert(math.min(table.unpack(lengths)) == 141)
assert(math.max(table.unpack(lengths)) == 736)
