local utils = require('utils')


local function evaluate(characters)
    local characters = characters:gsub('"(.*)"', '%1')
    characters = characters:gsub('\\x%x%x', 'x')
    characters = characters:gsub([[\(["\])]], '%1')
    return characters
end

local function escape(characters)
    return '"' .. characters:gsub([[(["\])]], [[\%1]]) .. '"'
end

local function literal_minus_memory(set, procedure)
    local literal = 0
    local memory = 0
    for _, characters in ipairs(set) do
        if procedure == evaluate then
            literal = literal + #characters
            memory = memory + #procedure(characters)
        else
            memory = memory + #characters
            literal = literal + #procedure(characters)
        end
    end
    return literal - memory
end


local tests = utils.read_file_multi('inputs/day8_test.txt')
assert(literal_minus_memory(tests, evaluate) == 12)
assert(literal_minus_memory(tests, escape) == 19)

local cases = utils.read_file_multi('inputs/day8.txt')
assert(literal_minus_memory(cases, evaluate) == 1350)
assert(literal_minus_memory(cases, escape) == 2085)
