local utils = require('utils')


local function parse_happiness(happiness_maps)
    local map = {}
    for _, situation in ipairs(happiness_maps) do
        local person1, operation, units, person2 = situation:match('(%a+) would (%a+) (%d+).* (%a+).')
        if not map[person1] then map[person1] = {} end
        operation = operation == 'gain' and '' or '-'
        map[person1][person2] = tonumber(operation..units)
    end
    return map
end

local function calculate_delta(arrangement, happiness_map)
    local delta = 0
    for i=1, #arrangement do
        local next_index = i==#arrangement and 1 or i+1
        if happiness_map[arrangement[i]] and happiness_map[arrangement[next_index]] then
            delta = delta + happiness_map[arrangement[i]][arrangement[next_index]] + happiness_map[arrangement[next_index]][arrangement[i]]
        end
    end
    return delta
end

local function optimal_arrangement_delta(attendees, happiness_map)
    local deltas = {}
    for arrangement in utils.permutations(attendees) do
        table.insert(deltas, calculate_delta(arrangement, happiness_map))
    end
    return math.max(table.unpack(deltas))
end


local test = {
    [[Alice would gain 54 happiness units by sitting next to Bob.]],
    [[Alice would lose 79 happiness units by sitting next to Carol.]],
    [[Alice would lose 2 happiness units by sitting next to David.]],
    [[Bob would gain 83 happiness units by sitting next to Alice.]],
    [[Bob would lose 7 happiness units by sitting next to Carol.]],
    [[Bob would lose 63 happiness units by sitting next to David.]],
    [[Carol would lose 62 happiness units by sitting next to Alice.]],
    [[Carol would gain 60 happiness units by sitting next to Bob.]],
    [[Carol would gain 55 happiness units by sitting next to David.]],
    [[David would gain 46 happiness units by sitting next to Alice.]],
    [[David would lose 7 happiness units by sitting next to Bob.]],
    [[David would gain 41 happiness units by sitting next to Carol.]],
}

local happiness_map = parse_happiness(test)
local attendees = {}
for attendee, _ in pairs(happiness_map) do table.insert(attendees, attendee) end
assert(optimal_arrangement_delta(attendees, happiness_map) == 330)

happiness_map = parse_happiness(utils.read_file_multi('inputs/day13.txt'))
attendees = {}
for attendee, _ in pairs(happiness_map) do table.insert(attendees, attendee) end
assert(optimal_arrangement_delta(attendees, happiness_map) == 664)
table.insert(attendees, 'Me')
assert(optimal_arrangement_delta(attendees, happiness_map) == 640)
