local utils = require'utils'

local function count_floors(parentheses)
    local remaining, count = parentheses:gsub('%(', '')
    return count - #remaining
end

local function first_negative(parentheses)
    local floor = 0
    for i=1, #parentheses do
        if parentheses:sub(i, i) == '(' then
            floor = floor + 1
        else
            floor = floor - 1
        end
        if floor == -1 then
            return i
        end
    end
end

local part_1_tests = {
    ['(())'] = 0,
    ['()()'] = 0,
    ['((('] = 3,
    ['(()(()('] = 3,
    ['))((((('] = 3,
    ['())'] = -1,
    ['))('] = -1,
    [')))'] = -3,
    [')())())'] = -3,
}

for test, floors in pairs(part_1_tests) do
    assert(count_floors(test) == floors)
end

local part_2_tests = {
    [')'] = 1,
    ['()())'] = 5
}

for test, floor in pairs(part_2_tests) do
    assert(first_negative(test) == floor)
end

local parentheses = utils.read_file_single('inputs/day1.txt')
assert(count_floors(parentheses) == 232)
assert (first_negative(parentheses) == 1783)
