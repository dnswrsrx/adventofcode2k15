local utils = require('utils')


local function sequential_in_sequence(char, sequence)
    local sequential = char

    for i=string.byte(char)+1, string.byte(char)+2 do
        sequential = sequential .. string.char(i)
    end

    return sequence:find(sequential) and true or false
end

local function check_using_set(set, sequence)
    local sequential = false
    local two_pairs = 0

    for char, _ in pairs(set) do
        if two_pairs ~= 2 then
            if sequence:find(char..char) then two_pairs = two_pairs + 1 end
        end

        if not sequential then sequential = sequential_in_sequence(char, sequence) end

        if sequential and two_pairs == 2 then return true end
    end
    return false
end

local function validate_password(sequence)
    return not sequence:find('[iol]') and check_using_set(utils.unique(sequence), sequence)
end

local function increment_password(sequence, index)
    index = index or #sequence
    local tabled = {}
    sequence:gsub('.', function(s) table.insert(tabled,s) end)

    if tabled[index] == 'z' then
        -- if index == 1 then return 'aaaaaaaa' end

        tabled[index] = 'a'
        return increment_password(table.concat(tabled, ''), index-1)
    else
        tabled[index] = string.char(string.byte(tabled[index]) + 1)
    end

    return table.concat(tabled, '')
end

local function find_valid_password(password)
    password = increment_password(password)
    while not validate_password(password) do
password = increment_password(password)
    end
    return password
end


local tests = {
    hijklmmn = false,
    abbceffg = false,
    abbcegjk = false,
}

for sequence, valid in pairs(tests) do
    assert(validate_password(sequence) == valid)
end

assert(find_valid_password('abcdefgh') == 'abcdffaa')
assert(find_valid_password('ghijklmn') == 'ghjaabcc')
assert(find_valid_password('vzbxkghb') == 'vzbxxyzz')
assert(find_valid_password('vzbxxyzz') == 'vzcaabcc')
