local function look_and_say(sequence)
    local next_sequence = {}
    local count = 1

    for i, char in ipairs(sequence) do
        if i ~= #sequence and char == sequence[i+1] then
            count = count + 1
        else
            table.insert(next_sequence, tostring(count))
            table.insert(next_sequence, char)
            count = 1
        end
    end
    return next_sequence
end

local function iterate(sequence, times)
    local tabled = {}
    sequence:gsub('.', function(s) table.insert(tabled, s) end)

    local iteration = 0

    repeat
        tabled = look_and_say(tabled)
        iteration = iteration + 1
    until iteration == times

    return tabled
end


local tests = {
    ['1'] = '11',
    ['11'] = '21',
    ['21'] = '1211',
    ['1211'] = '111221',
    ['111221'] = '312211'
}

for sequence, next_sequence in pairs(tests) do
    assert(table.concat(iterate(sequence, 1), '') == next_sequence)
end

local input = '1113122113'
assert(#iterate(input, 40) == 360154)
assert(#iterate(input, 50) == 5103798)

